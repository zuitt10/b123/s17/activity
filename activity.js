let registeredStudents = [
	"James Jeffries",
	"Gunther Smith",
	"Macie West",
	"Michelle Queen",
	"Shane Miguelito",
	"Fernando Dela Cruz",
	"Akiko Yukihime"
];

let section1 = [];
let section2 = [];
let section3 = [];

// Add/pushing a student in variable registeredStudents

function register(user){
	let doesStudentExist = registeredStudents.includes(user);

	if (doesStudentExist) {
		console.log("Registration failed. student already exist!")
	} else {
		registeredStudents.push(user);
		console.log(`Thank you for registering ${user}!`)
	}
};


// Adding/pusing a student and it's section array

function addStudent (user, section){
	let addRegisteredStudent = registeredStudents.find(function(username){
		return user === username;
	});

	if (addRegisteredStudent && section == 1) {
		section1.push(addRegisteredStudent);
		console.log(`You have added ${addRegisteredStudent} to Section 1`)
	} else if(addRegisteredStudent && section == 2){
		section2.push(addRegisteredStudent);
		console.log(`You have added ${addRegisteredStudent} to Section 2`)
	} else if(addRegisteredStudent && section == 3){
		section3.push(addRegisteredStudent);
		console.log(`You have added ${addRegisteredStudent} to Section 3`)
	} else {
		console.log("Student not found")
	}
};


// Display the name of the students one by one in the specific section array.

function displaySection1 (){
	if(section1.length<0){
		section1.forEach(function(students){

			console.log(students)
		})
	} else {
		console.log(`Section 1 has ${section1.length} students. Add one first.`)
	}

}

function displaySection2 (){
	if(section2.length > 0){
		section2.forEach(function(students){

			console.log(students)
		})

	} else {
		console.log(`Section 2 have ${section2.length} students. Add one first.`)
	}

}

function displaySection3 (){
	if(section3.length > 0){
		section3.forEach(function(students){

			console.log(students)
		})
	} else {
		console.log(`Section 3 have ${section3.length} students. Add one first.`)
	}

}

// Check Number of Student in the specific section array.

function displayNumSection1 (){
	if (section1.length > 0) {

		console.log(`You currently have ${section1.length} students in  Section 1`)
	} else{

		console.log(`You currently have ${section1.length} students. Please add a student.`)
	}
}


function displayNumSection2 (){
	if (section2.length > 0) {

		console.log(`You currently have ${section2.length} students in  Section 2`)
	} else{

		console.log(`You currently have ${section2.length} students. Please add a student.`)
	}
}

function displayNumSection3 (){
	if (section3.length > 0) {

		console.log(`You currently have ${section3.length} students in  Section 3`)
	} else{

		console.log(`You currently have ${section3.length} students. Please add a student.`)
	}
}